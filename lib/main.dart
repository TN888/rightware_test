import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rightware_test/global/routes/routes_names.dart';
import 'package:rxdart/rxdart.dart';

import 'global/apis/user_api.dart';
import 'global/localization/language_changer.dart';
import 'global/localization/localization.dart';
import 'global/routes/routes_generator.dart';
import 'global/utils/colors.dart';
import 'global/utils/font_sizes.dart';
import 'global/widgets/screen_size_builder.dart';

PublishSubject<bool> materialAppStreamController = PublishSubject<bool>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  final userApi = UserApi();

  final language = await setAppLanguage(userApi);
  final languageChanger = LanguageChanger(language);
  await languageChanger.getLanguage();

  runApp(
    MyApp(
      userApi: userApi,
      languageChanger: languageChanger,
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key key,
    @required this.userApi,
    @required this.languageChanger,
  })  : assert(userApi != null),
        assert(languageChanger != null),
        super(key: key);

  final UserApi userApi;
  final LanguageChanger languageChanger;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (context) => userApi),
        RepositoryProvider(create: (context) => languageChanger),
      ],
      child: StreamBuilder<bool>(
        stream: materialAppStreamController.stream,
        builder: (context, snapshot) {
          return MaterialApp(
            onGenerateTitle: (context) {
              return getTranslate(context, 'app_name');
            },
            debugShowCheckedModeBanner: false,
            supportedLocales: [
              Locale(languages['arabic'].code),
              Locale(languages['english'].code),
            ],
            locale: Locale(languageChanger.language),
            localizationsDelegates: const [
              AppLocalizationsDelegate(),
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              DefaultCupertinoLocalizations.delegate
            ],
            theme: ThemeData(
              primarySwatch: Colors.red,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              backgroundColor: Colors.white,
            ),
            builder: (context, child) {
              return ScreenSizeBuilder(
                callBack: () => init(ScreenSizeBuilder.size),
                textScaleFactor: 1,
                child: child,
              );
            },
            initialRoute: restaurantMenu,
            onGenerateRoute: allRoutes,
          );
        },
      ),
    );
  }
}

Future<String> setAppLanguage(UserApi userApi) async {
  String languageVar;
  try {
    return await userApi.getKey(setLanguage) as String;
  } catch (e) {
    if (Platform.localeName.substring(0, 2) == languages['arabic'].code) {
      languageVar = languages['arabic'].code;
    } else {
      languageVar = languages['english'].code;
    }
  }
  return languageVar;
}
