import 'package:rightware_test/global/apis/user_api.dart';
import 'package:rightware_test/global/localization/localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rightware_test/restaurant_menu/cubit/restaurant_menu_cubit.dart';
import 'package:rightware_test/restaurant_menu/restaurant_menu_api.dart';
import 'package:rightware_test/restaurant_menu/screen/restaurant_menu_screen.dart';

import 'routes_names.dart';

Route<dynamic> allRoutes(RouteSettings settings) {
  debugPrint('materialApp.onGenerateRoute');
  switch (settings.name) {
    case restaurantMenu:
      return MaterialPageRoute(
        builder: (context) {
          return BlocProvider(
            create: (context) => RestaurantMenuCubit(
              RepositoryProvider.of<UserApi>(context),
              RestaurantMenuApi(),
            ),
            child: const RestaurantMenuScreen(),
          );
        },
      );
    default:
      return MaterialPageRoute(
        builder: (context) => Center(
          child: Text(
            getTranslate(context, 'went_wrong'),
          ),
        ),
      );
  }
}
