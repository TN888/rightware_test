import 'package:shared_preferences/shared_preferences.dart';

const setLanguage = 'set language';

class UserApi {
  factory UserApi() {
    return _instance;
  }

  UserApi._() : properties = {};

  static final UserApi _instance = UserApi._();

  Map<String, dynamic> properties;

  Future<bool> setKey<T>(String key, T value) async {
    final prefs = await SharedPreferences.getInstance();
    bool result = false;

    if (value != null) {
      if (value is bool) {
        result = await prefs.setBool(key, value);
      } else if (value is double) {
        result = await prefs.setDouble(key, value);
      } else if (value is int) {
        result = await prefs.setInt(key, value);
      } else if (value is String) {
        result = await prefs.setString(key, value);
      } else if (value is List<String>) {
        result = await prefs.setStringList(key, value);
      } else {
        throw Exception(
            'The value type is not supported in SharedPreferences.');
      }
    } else {
      throw Exception("Value can't be null.");
    }

    if (result) properties[key] = value;
    return result;
  }

  Future getKey(String key, {dynamic defaultValue}) async {
    final prefs = await SharedPreferences.getInstance();

    if (!prefs.containsKey(key) && defaultValue == null) {
      throw Exception(
          "The $key key isn't found in SharedPreferences, so default value can't be null.");
    } else if (!prefs.containsKey(key)) {
      final result = await setKey(key, defaultValue);
      if (!result) throw Exception("Default value can't be set.");
    }

    if (defaultValue is bool) {
      properties[key] = prefs.getBool(key);
    } else if (defaultValue is double) {
      properties[key] = prefs.getDouble(key);
    } else if (defaultValue is int) {
      properties[key] = prefs.getInt(key);
    } else if (defaultValue is String) {
      properties[key] = prefs.getString(key);
    } else if (defaultValue is List<String>) {
      properties[key] = prefs.getStringList(key);
    } else {
      properties[key] = prefs.get(key);
    }
    return properties[key];
  }

  Future<bool> removeKey(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }
}
