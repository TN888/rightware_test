import 'dart:convert';
import 'dart:io';

import 'package:rightware_test/global/localization/localization.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

enum RequestType { get, post, delete }

class BaseApi {
  Future<http.Response> loadData(
      BuildContext context, RequestType requestType, Uri uri,
      {String token, Map body}) async {
    try {
      http.Response response;
      if (requestType == RequestType.get) {
        response = await _getRequest(uri, headers: _setHeaders(token));
      } else if (requestType == RequestType.post) {
        response = await _postRequest(uri, body, headers: _setHeaders(token));
      } else if (requestType == RequestType.delete) {
        response = await _deleteRequest(uri, headers: _setHeaders(token));
      }

      return response;
    } on SocketException {
      throw Exception(getTranslate(context, 'no_connection'));
    }
  }

  Future<http.Response> _getRequest(Uri url,
      {Map<String, String> headers}) async {
    final response = await http.get(url, headers: headers);
    return response;
  }

  Future<http.Response> _postRequest(Uri url, Map body,
      {Map<String, String> headers}) async {
    final response =
        await http.post(url, headers: headers, body: json.encode(body));
    return response;
  }

  Future<http.Response> _deleteRequest(Uri url,
      {Map<String, String> headers}) async {
    final response = await http.delete(url, headers: headers);
    return response;
  }

  Map<String, String> _setHeaders(String token) {
    final headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };

    if (token != null) {
      headers['secretKey'] = token;
    }
    return headers;
  }
}
