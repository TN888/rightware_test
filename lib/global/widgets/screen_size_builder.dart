import 'package:flutter/material.dart';

enum ScreenSizes {
  extraLarge,
  large,
  medium,
  small,
  extraSmall,
  twoExtraSmall,
}

class ScreenSizeBuilder extends StatelessWidget {
  const ScreenSizeBuilder({
    @required this.child,
    this.textScaleFactor,
    @required this.callBack,
  })  : assert(child != null),
        assert(callBack != null);

  final Widget child;
  final double textScaleFactor;
  final Function() callBack;

  static ScreenSizes size;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth >= 480) {
          size = ScreenSizes.extraLarge;
        } else if (constraints.maxWidth >= 414) {
          size = ScreenSizes.large;
        } else if (constraints.maxWidth >= 375) {
          size = ScreenSizes.medium;
        } else if (constraints.maxWidth >= 360) {
          size = ScreenSizes.small;
        } else if (constraints.maxWidth >= 320) {
          size = ScreenSizes.extraSmall;
        } else {
          size = ScreenSizes.twoExtraSmall;
        }

        callBack();
        return MediaQuery(
          data:
              MediaQuery.of(context).copyWith(textScaleFactor: textScaleFactor),
          child: child,
        );
      },
    );
  }
}
