import 'package:rightware_test/global/utils/font_sizes.dart';
import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final String title;
  final Color color;
  final VoidCallback onTap;

  const ActionButton({
    @required this.title,
    this.color,
    @required this.onTap,
  })  : assert(title != null),
        assert(onTap != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: color,
      ),
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          borderRadius: BorderRadius.circular(20),
          onTap: onTap,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Text(
                title,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: text,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
