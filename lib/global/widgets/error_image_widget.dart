import 'package:flutter/material.dart';

class ErrorImageWidget extends StatelessWidget {
  const ErrorImageWidget({
    Key key,
    this.margin = const EdgeInsets.all(12),
    @required this.imagePath,
  })  : assert(imagePath != null),
        super(key: key);

  final EdgeInsetsGeometry margin;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: Image.asset(
        imagePath,
        height: 100,
        fit: BoxFit.contain,
      ),
    );
  }
}
