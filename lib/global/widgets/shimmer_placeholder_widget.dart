import 'package:rightware_test/global/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerPlaceHolderWidget extends StatelessWidget {
  const ShimmerPlaceHolderWidget({
    Key key,
    this.height,
    this.radius = 15,
  }) : super(key: key);

  final double height;
  final double radius;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: Shimmer.fromColors(
        baseColor: Colors.grey.shade100,
        highlightColor: Colors.red.shade100,
        child: Container(
          height: height,
          color: Colors.white,
        ),
      ),
    );
  }
}
