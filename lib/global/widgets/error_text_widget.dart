import 'package:rightware_test/global/localization/localization.dart';
import 'package:rightware_test/global/utils/font_sizes.dart';
import 'package:flutter/material.dart';

class ErrorText extends StatelessWidget {
  const ErrorText(
    this.message, {
    this.withHeight = true,
    this.height,
    this.fontSize,
    this.tryAgainButton = false,
    this.tryAgainOnTap,
  });

  final String message;
  final bool withHeight;
  final double height;
  final double fontSize;
  final bool tryAgainButton;
  final VoidCallback tryAgainOnTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      height: withHeight ? height ?? MediaQuery.of(context).size.height : null,
      alignment: withHeight ? const Alignment(0, -0.7) : null,
      child: Wrap(
        alignment: WrapAlignment.center,
        runSpacing: 4,
        children: [
          Text(
            message,
            style: TextStyle(
              color: Colors.grey,
              fontSize: fontSize ?? text,
            ),
            textAlign: TextAlign.center,
          ),
          if (tryAgainButton)
            Text(
              ',',
              style: TextStyle(
                color: Colors.grey,
                fontSize: fontSize ?? text,
              ),
              textAlign: TextAlign.center,
            ),
          if (tryAgainButton)
            //ignore: missing_required_param
            InkWell(
              onTap: tryAgainOnTap,
              child: Container(
                color: Colors.white,
                child: Text(
                  getTranslate(context, 'try_again'),
                  style: const TextStyle(
                    decoration: TextDecoration.underline,
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}
