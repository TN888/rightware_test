import 'package:rightware_test/global/localization/localization.dart';
import 'package:rightware_test/global/utils/font_sizes.dart';
import 'package:flutter/material.dart';

class MyErrorWidget extends StatelessWidget {
  const MyErrorWidget({
    Key key,
    @required this.message,
    this.height,
    this.fontSize,
    this.onTap,
  })  : assert(message != null),
        super(key: key);

  final String message;
  final double height;
  final double fontSize;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      height: height ?? MediaQuery.of(context).size.height,
      alignment: const Alignment(0, -0.7),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            message,
            style: TextStyle(
              color: Colors.grey,
              fontSize: fontSize ?? text,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 12),
          //ignore: missing_required_param
          Material(
            type: MaterialType.transparency,
            child: InkWell(
              onTap: onTap,
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 9, horizontal: 15),
                decoration: BoxDecoration(
                  // color: Colors.grey,
                  borderRadius: BorderRadius.circular(25),
                  border: Border.all(width: 2),
                ),
                child: Text(
                  getTranslate(context, 'retry'),
                  style: const TextStyle(
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
