import 'package:rightware_test/global/utils/colors.dart';
import 'package:rightware_test/global/utils/utils.dart';
import 'package:flutter/material.dart';

void showMySnackBar(BuildContext context, String text,
    {Duration delay = Duration.zero}) {
  setStatusBarColor(Colors.transparent, Brightness.dark);
  Future.delayed(
    delay,
    () {
      showModalBottomSheet(
        context: context,
        useRootNavigator: true,
        backgroundColor: Colors.white,
        barrierColor: bottomSheetBarrierColor,
        builder: (context) {
          return ListTile(
            title: Text(text),
          );
        },
      );
    },
  );
}
