import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MyAppBar({
    Key key,
    this.automaticallyImplyLeading = true,
    this.leading,
    this.title,
    this.centerTitle = false,
    this.brightness,
    this.actions = const [],
    this.bottom,
  }) : super(key: key);

  final bool automaticallyImplyLeading;
  final Widget leading;
  final Widget title;
  final bool centerTitle;
  final Brightness brightness;
  final List<Widget> actions;
  final PreferredSizeWidget bottom;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: AppBar(
        brightness: brightness,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: automaticallyImplyLeading,
        leading: leading,
        centerTitle: centerTitle,
        title: title,
        actions: [
          Align(
            alignment: const AlignmentDirectional(1, 0),
            child: Padding(
              padding: const EdgeInsetsDirectional.only(end: 16.0),
              child: Row(
                children: actions,
              ),
            ),
          ),
        ],
        // actions: actions,
        bottom: bottom,
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
