import 'package:flutter/material.dart';
import 'package:rightware_test/global/widgets/my_scroll_configuration.dart';

import 'error_text_widget.dart';

class ScrollableErrorText extends StatelessWidget {
  const ScrollableErrorText(
    this.text, {
    this.scrollController,
    Key key,
    this.height,
    this.withErrorTextHeight = true,
    this.errorTextHeight = 200,
    this.physics,
    this.child = const SizedBox(),
  })  : assert(text != null),
        super(key: key);

  final ScrollController scrollController;
  final String text;
  final bool withErrorTextHeight;
  final double height;
  final double errorTextHeight;
  final ScrollPhysics physics;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MyScrollConfiguration(
      child: SingleChildScrollView(
        controller: scrollController,
        physics: physics ?? const AlwaysScrollableScrollPhysics(),
        child: Container(
          height: height ?? MediaQuery.of(context).size.height,
          alignment: const Alignment(0, -0.7),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ErrorText(
                text,
                height: errorTextHeight,
                withHeight: withErrorTextHeight,
              ),
              child,
            ],
          ),
        ),
      ),
    );
  }
}
