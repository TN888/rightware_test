import 'package:rightware_test/global/widgets/screen_size_builder.dart';

double title;
double subtitle;
double bigText;
double text;
double smallText;
double notice;

void init(ScreenSizes size) {
  if (size == ScreenSizes.extraLarge) {
    title = 35.0;
    subtitle = 33.5;
    bigText = 32.5;
    text = 28.5;
    smallText = 24.5;
    notice = 20.0;
  } else if (size == ScreenSizes.large) {
    title = 32.0;
    subtitle = 28.0;
    bigText = 27.0;
    text = 24.0;
    smallText = 21.5;
    notice = 19.0;
  } else if (size == ScreenSizes.medium) {
    title = 28.0;
    subtitle = 25.0;
    bigText = 24.5;
    text = 21.5;
    smallText = 19.5;
    notice = 17.5;
  } else if (size == ScreenSizes.small) {
    title = 26.0;
    subtitle = 24.0;
    bigText = 23.5;
    text = 20.0;
    smallText = 18.5;
    notice = 15.0;
  } else if (size == ScreenSizes.extraSmall) {
    title = 24.0;
    subtitle = 22.0;
    bigText = 21.0;
    text = 19.0;
    smallText = 17.5;
    notice = 14.5;
  } else {
    title = 21.0;
    subtitle = 19.0;
    bigText = 18.0;
    text = 16.0;
    smallText = 15.0;
    notice = 13.0;
  }
}
