const String baseUrl = 'http://192.168.1.9:8000';
const String imagesBaseUrl = '$baseUrl/storage/';

const String logoUrl = 'https://www.orianosy.com/splash/img/light-1x.png';

const String logoIcon = 'assets/icons/logo.png';

//HTTP status codes
const int successCode = 200;
const int createdCode = 201;
const int notFoundCode = 404;
const int unprocessableEntityCode = 422;
