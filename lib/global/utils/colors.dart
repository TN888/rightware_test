import 'package:flutter/material.dart';

//General
const int _mainColorValue = 0xFF0F1626;
const MaterialColor mainColor = MaterialColor(
  _mainColorValue,
  {
    50: Color(0xFF4C72C7),
    100: Color(0xFF3E5DA3),
    200: Color(0xFF2F467B),
    300: Color(0xFF223257),
    400: Color(0xFF17223B),
    500: Colors.red,
    600: Color(0xFF0A101D),
    700: Color(0xFF070D18),
    800: Color(0xFF070E1A),
    900: Color(0xFF060B17),
  },
);

const Color secondaryColor = Color(0xFFFF533D);
final shimmerBackgroundColor = Colors.grey.shade100;
const shimmerHighlightColor = Color(0xFFC5C8D7);
const loadingIndicatorColor = mainColor;
final bottomSheetBarrierColor = mainColor.withOpacity(0.45);

/// String is in the format "aabbcc", "AABBCC" or "ffaabbcc" with an optional leading "#".
Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}
