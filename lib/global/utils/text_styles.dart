import 'package:rightware_test/global/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle getWhite(double fontSize) => TextStyle(
      fontSize: fontSize,
      color: Colors.white,
    );

TextStyle getWhiteBold(double fontSize) => TextStyle(
      fontSize: fontSize,
      color: Colors.white,
      fontWeight: FontWeight.bold,
    );

TextStyle getWhiteBoldUnderlined(double fontSize) => TextStyle(
      fontSize: fontSize,
      decoration: TextDecoration.underline,
      color: Colors.white,
      fontWeight: FontWeight.bold,
    );

TextStyle getStyle(double fontSize, {Color color = mainColor}) => TextStyle(
      fontSize: fontSize,
      color: color,
    );

TextStyle getBold(double fontSize, {Color color = Colors.black}) => TextStyle(
      fontSize: fontSize,
      color: color,
      fontWeight: FontWeight.bold,
    );

TextStyle getYellowBold(double fontSize) => TextStyle(
      fontSize: fontSize,
      color: Colors.yellow,
    );

TextStyle getBlack(double fontSize) => TextStyle(
      fontSize: fontSize,
      color: Colors.black,
    );

TextStyle getWhiteWithOpacity(double fontSize) => TextStyle(
      fontSize: fontSize,
      color: Colors.white.withOpacity(0.5),
    );
