import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'language_changer.dart';

String getTranslate(BuildContext context, String key) {
  return Localization.of(context).trans(key);
}

class Localization {
  Localization(this.locale);

  final Locale locale;

  static Localization of(BuildContext context) {
    return Localizations.of<Localization>(context, Localization);
  }

  Map<String, String> _sentences;

  Future<bool> load() async {
    final data = await rootBundle.loadString(_setTranslationsPath(locale.languageCode));
    final _result = json.decode(data);

    _sentences = {};
    _result.forEach((String key, dynamic value) {
      _sentences[key] = value.toString();
    });
    return true;
  }

  String trans(String key) {
    return _sentences[key] ?? '';
  }

  String _setTranslationsPath(String languageCode) {
    return 'assets/lang/$languageCode/translations.json';
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<Localization> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    for (var i = 0; i < languages.values.length; i++) {
      if (languages.values.toList()[i].code == locale.languageCode) return true;
    }
    return false;
  }

  @override
  Future<Localization> load(Locale locale) async {
    final localizations = Localization(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
