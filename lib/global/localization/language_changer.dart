import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String languageKey = 'language key';

const Map<String, LanguageModel> languages = {
  'arabic': LanguageModel('العربية', 'ar'),
  'english': LanguageModel('English', 'en'),
};

bool isLanguage(BuildContext context, String language) {
  return RepositoryProvider.of<LanguageChanger>(context).language ==
      languages[language].code;
}

class LanguageChanger {
  factory LanguageChanger(String defaultLanguage) {
    return _instance ??= LanguageChanger._(defaultLanguage);
  }

  LanguageChanger._(this._defaultLanguage);

  static LanguageChanger _instance;

  String _language;

  final String _defaultLanguage;

  Future<bool> setLanguage(String language) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final result = await prefs.setString(languageKey, language);

    if (result) _language = language;
    return result;
  }

  Future<String> getLanguage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return _language = prefs.getString(languageKey) ?? _defaultLanguage;
  }

  String get language => _language;

  String convertLanguageCodeToName(String language) {
    for (var i = 0; i < languages.values.length; i++) {
      if (languages.values.toList()[i].code == language)
        return languages.values.toList()[i].name;
    }
    throw Exception("Language name isn't found");
  }
}

class LanguageModel {
  const LanguageModel(this.name, this.code);

  final String name;
  final String code;
}
