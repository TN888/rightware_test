import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:rightware_test/global/apis/user_api.dart';
import 'package:rightware_test/global/localization/localization.dart';
import 'package:rightware_test/global/utils/constants.dart';
import 'package:rightware_test/restaurant_menu/restaurant_menu_api.dart';
import 'package:rightware_test/restaurant_menu/restaurant_menu_model.dart';

part 'restaurant_menu_state.dart';

class RestaurantMenuCubit extends Cubit<RestaurantMenuState> {
  RestaurantMenuCubit(
    this.userApi,
    this.restaurantMenuApi,
  ) : super(RestaurantMenuInitial());

  final UserApi userApi;
  final RestaurantMenuApi restaurantMenuApi;

  Future<void> getRestaurantMenu(BuildContext context) async {
    emit(RestaurantMenuLoading());

    debugPrint('rightware');

    //These are fixed for now.
    const token = 'TsTForBLiveOoOo';
    const menuId = '605c9e18a6069d09ae005024';

    // try {
    final response =
        await restaurantMenuApi.getRestaurantMenu(context, token, menuId);

    if (response.statusCode == successCode) {
      final restaurantMenuModel = RestaurantMenuModel.fromJson(response.body);

      emit(
        RestaurantMenuSuccess(
          restaurantMenuModel,
          isEmpty: restaurantMenuModel.menuCategories.isEmpty,
        ),
      );
    } else {
      debugPrint('else');
      emit(
        RestaurantMenuError(
          '${getTranslate(context, 'went_wrong')}\n ${getTranslate(context, 'swipe_from_top')}',
        ),
      );
    }
    // } catch (e) {
    //   debugPrint('Error ${e.toString()}');
    //   emit(
    //     RestaurantMenuError(
    //       '${getTranslate(context, 'went_wrong')}\n ${getTranslate(context, 'swipe_from_top')}',
    //     ),
    //   );
    // }
  }
}
