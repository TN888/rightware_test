part of 'restaurant_menu_cubit.dart';

@immutable
abstract class RestaurantMenuState extends Equatable {}

class RestaurantMenuInitial extends RestaurantMenuState {
  @override
  List<Object> get props => [];
}

class RestaurantMenuSuccess extends RestaurantMenuState {
  RestaurantMenuSuccess(this.restaurantMenuModel, {this.isEmpty = false});
  final RestaurantMenuModel restaurantMenuModel;
  final bool isEmpty;

  @override
  List<Object> get props => [restaurantMenuModel];
}

class RestaurantMenuLoading extends RestaurantMenuState {
  @override
  List<Object> get props => [];
}

class RestaurantMenuError extends RestaurantMenuState {
  RestaurantMenuError(this.message);

  final String message;

  @override
  List<Object> get props => [];
}
