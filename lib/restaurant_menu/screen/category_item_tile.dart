import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rightware_test/global/utils/colors.dart';
import 'package:rightware_test/global/utils/constants.dart';
import 'package:rightware_test/global/utils/font_sizes.dart';
import 'package:rightware_test/global/utils/text_styles.dart';
import 'package:rightware_test/global/widgets/error_image_widget.dart';
import 'package:rightware_test/global/widgets/shimmer_placeholder_widget.dart';

import '../restaurant_menu_model.dart';

class CategoryItemTile extends StatefulWidget {
  const CategoryItemTile({
    Key key,
    @required this.categoryItem,
    this.isArabic = false,
    @required this.onTap,
  })  : assert(categoryItem != null),
        assert(onTap != null),
        super(key: key);

  final CategoryItem categoryItem;
  final bool isArabic;
  final VoidCallback onTap;

  @override
  State<CategoryItemTile> createState() => _CategoryItemTileState();
}

class _CategoryItemTileState extends State<CategoryItemTile> {
  bool showOptions = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          showOptions = !showOptions;
        });
        widget.onTap();
      },
      borderRadius: BorderRadius.circular(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: CachedNetworkImage(
                imageUrl: widget.categoryItem.photo,
                placeholder: (context, url) =>
                    const ShimmerPlaceHolderWidget(height: 150),
                errorWidget: (context, url, error) => Container(
                  color: Colors.red.shade300,
                  child: const ErrorImageWidget(imagePath: logoIcon),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.only(top: 10.0, start: 4),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.isArabic
                            ? widget.categoryItem.itemName.ar
                            : widget.categoryItem.itemName.en,
                        textAlign: TextAlign.start,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: getBold(text).copyWith(height: 1.4),
                      ),
                      Padding(
                        padding: const EdgeInsetsDirectional.only(top: 6.0),
                        child: Text(
                          '${widget.categoryItem.price.amount} ${widget.categoryItem.price.currency}',
                          maxLines: 1,
                          textAlign: TextAlign.start,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: mainColor,
                            fontWeight: FontWeight.w600,
                            fontSize: smallText,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                if (widget.categoryItem.option.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(
                      showOptions
                          ? Icons.keyboard_arrow_up_rounded
                          : Icons.keyboard_arrow_down_rounded,
                    ),
                  ),
              ],
            ),
          ),
          AnimatedSwitcher(
            duration: const Duration(milliseconds: 200),
            transitionBuilder: (child, animation) {
              return SizeTransition(sizeFactor: animation, child: child);
            },
            child: showOptions
                ? ListView.separated(
                    padding: const EdgeInsets.only(
                        left: 10, top: 20, right: 10, bottom: 10),
                    shrinkWrap: true,
                    itemCount: widget.categoryItem.option.length,
                    itemBuilder: (context, index) {
                      return Row(
                        children: [
                          Text(
                            widget.isArabic
                                ? widget.categoryItem.option[index].ar
                                : widget.categoryItem.option[index].en,
                          ),
                          const SizedBox(width: 20),
                          Text(
                            '${widget.categoryItem.option[index].price.amount} ${widget.categoryItem.option[index].price.currency}',
                            style: const TextStyle(color: Colors.grey),
                          ),
                        ],
                      );
                    },
                    separatorBuilder: (context, index) {
                      return const SizedBox(height: 10);
                    },
                  )
                : const SizedBox(),
          ),
        ],
      ),
    );
  }
}
