import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rightware_test/global/localization/language_changer.dart';
import 'package:rightware_test/global/localization/localization.dart';
import 'package:rightware_test/global/utils/font_sizes.dart';
import 'package:rightware_test/global/utils/text_styles.dart';
import 'package:rightware_test/global/widgets/my_scroll_configuration.dart';
import 'package:rightware_test/global/widgets/scrollable_error_text.dart';
import 'package:rightware_test/restaurant_menu/cubit/restaurant_menu_cubit.dart';
import 'package:rightware_test/restaurant_menu/screen/category_item_tile.dart';

class RestaurantMenuScreen extends StatefulWidget {
  const RestaurantMenuScreen({Key key}) : super(key: key);

  @override
  _RestaurantMenuScreenState createState() => _RestaurantMenuScreenState();
}

class _RestaurantMenuScreenState extends State<RestaurantMenuScreen>
    with TickerProviderStateMixin {
  RestaurantMenuCubit restaurantMenuCubit;
  bool isArabic;

  TabController tabController;

  @override
  void initState() {
    super.initState();

    restaurantMenuCubit = BlocProvider.of<RestaurantMenuCubit>(context);
    restaurantMenuCubit.getRestaurantMenu(context);

    isArabic = isLanguage(context, 'arabic');
  }

  @override
  void dispose() {
    tabController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RestaurantMenuCubit, RestaurantMenuState>(
      builder: (context, state) {
        PreferredSizeWidget bottom;
        Widget body;

        if (state is RestaurantMenuSuccess) {
          if (state.isEmpty) {
            body = ScrollableErrorText(
              getTranslate(context, 'empty'),
            );
          } else {
            tabController = TabController(
              length: state.restaurantMenuModel.menuCategories.length,
              vsync: this,
            );

            bottom = buildCategoriesBar(state);
            body = buildBody(state);
          }
        } else if (state is RestaurantMenuError) {
          body = ScrollableErrorText(state.message);
        } else if (state is RestaurantMenuLoading) {
          body = const Center(
            child: CircularProgressIndicator(),
          );
        }

        return Scaffold(
          appBar: buildAppBar(bottom: bottom),
          body: body,
        );
      },
    );
  }

  PreferredSizeWidget buildAppBar({PreferredSizeWidget bottom}) {
    return AppBar(
      backgroundColor: Colors.red.shade900,
      title: Text(
        getTranslate(context, 'restaurant_menu'),
        style: getBold(title, color: Colors.white),
      ),
      bottom: bottom,
    );
  }

  PreferredSizeWidget buildCategoriesBar(RestaurantMenuSuccess state) {
    return TabBar(
      controller: tabController,
      indicatorColor: Colors.white,
      indicatorWeight: 5,
      isScrollable: true,
      labelColor: Colors.white,
      labelStyle: getBold(smallText),
      tabs: state.restaurantMenuModel.menuCategories
          .map<Widget>(
            (e) => Tab(
              text: isArabic ? e.categoryName.ar : e.categoryName.en,
            ),
          )
          .toList(),
    );
  }

  Widget buildBody(RestaurantMenuSuccess state) {
    return MyScrollConfiguration(
      child: TabBarView(
        controller: tabController,
        children: state.restaurantMenuModel.menuCategories
            .map<Widget>(
              (e) => MyScrollConfiguration(
                child: ListView.separated(
                  key: PageStorageKey<String>(e.id),
                  shrinkWrap: true,
                  padding:
                      const EdgeInsets.symmetric(vertical: 22, horizontal: 16),
                  itemCount: e.item.length,
                  itemBuilder: (context, index) {
                    return CategoryItemTile(
                      categoryItem: e.item[index],
                      onTap: () {},
                    );
                  },
                  separatorBuilder: (context, index) {
                    return const SizedBox(height: 20);
                  },
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
