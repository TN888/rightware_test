import 'package:flutter/widgets.dart';
import 'package:rightware_test/global/apis/base_api.dart';

import 'package:http/http.dart' as http;

class RestaurantMenuApi extends BaseApi {
  Future<http.Response> getRestaurantMenu(
      BuildContext context, String token, String menuId) async {
    final uri = Uri.parse(
        'https://api.orianosy.com/menu/all/enable/available/category/v2?menuId=$menuId&deviceKind=required - string&fullName=PostIAssignmentM');
    return loadData(context, RequestType.get, uri, token: token);
  }
}
