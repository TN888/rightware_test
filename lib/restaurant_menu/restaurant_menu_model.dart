// To parse this JSON data, do
//
//     final restaurantMenuModel = restaurantMenuModelFromMap(jsonString);

import 'dart:convert';

import 'package:rightware_test/global/utils/constants.dart';

class RestaurantMenuModel {
  RestaurantMenuModel({
    this.menuCategories,
  });

  final List<MenuCategory> menuCategories;

  factory RestaurantMenuModel.fromJson(String str) =>
      RestaurantMenuModel.fromMap(json.decode(str));

  factory RestaurantMenuModel.fromMap(Map<String, dynamic> json) =>
      RestaurantMenuModel(
        menuCategories: json["result"] == null
            ? <MenuCategory>[]
            : List<MenuCategory>.from(
                json["result"].asMap().entries.map(
                      (x) => MenuCategory.fromMap(x.value, index: x.key),
                    ),
              ),
      );
}

class MenuCategory {
  MenuCategory({
    this.id,
    this.categoryName,
    this.description,
    this.index,
    this.item,
  });

  final String id;
  final BilingualText categoryName;
  final BilingualText description;
  final int index;
  final List<CategoryItem> item;

  factory MenuCategory.fromJson(String str) =>
      MenuCategory.fromMap(json.decode(str));

  factory MenuCategory.fromMap(Map<String, dynamic> json, {int index}) {
    if (json["_id"] == null) {
      throw Exception("Menu category id can't be null");
    }

    if (json["categoryName"] == null) {
      throw Exception("Menu category name can't be null");
    }

    return MenuCategory(
      id: json["_id"],
      categoryName: BilingualText.fromMap(json["categoryName"]),
      description: json["description"] == null
          ? BilingualText(ar: '', en: '')
          : BilingualText.fromMap(json["description"]),
      index: json["index"] ?? index,
      item: json["item"] == null
          ? <CategoryItem>[]
          : List<CategoryItem>.from(
              json["item"].asMap().entries.map(
                    (x) => CategoryItem.fromMap(x.value, index: x.key),
                  ),
            ),
    );
  }
}

class CategoryItem {
  CategoryItem({
    this.id,
    this.categoryId,
    this.itemName,
    this.price,
    this.option,
    this.photo,
    this.index,
    this.availability,
    this.enable,
    this.updatedAt,
    this.createdAt,
    this.description,
    this.note,
  });

  final String id;
  final String categoryId;
  final BilingualText itemName;
  final Price price;
  final List<Option> option;
  final String photo;
  final int index;
  final bool availability;
  final bool enable;
  final DateTime updatedAt;
  final DateTime createdAt;
  final BilingualText description;
  final BilingualText note;

  factory CategoryItem.fromJson(String str) =>
      CategoryItem.fromMap(json.decode(str));

  factory CategoryItem.fromMap(Map<String, dynamic> json, {int index}) {
    if (json["_id"] == null) {
      throw Exception("Category item id can't be null");
    }

    if (json["categoryId"] == null) {
      throw Exception("Menu category id of item can't be null");
    }

    if (json["itemName"] == null) {
      throw Exception("Category item name can't be null");
    }

    if (json["price"] == null) {
      throw Exception("Category item price can't be null");
    }

    return CategoryItem(
      id: json["_id"],
      categoryId: json["categoryId"],
      itemName: BilingualText.fromMap(json["itemName"]),
      price: Price.fromMap(json["price"]),
      option: json["option"] == null
          ? <Option>[]
          : List<Option>.from(json["option"].map((x) => Option.fromMap(x))),
      photo: json["photo"] ?? logoUrl,
      index: json["index"] ?? index,
      availability: json["availability"] ?? false,
      enable: json["enable"] ?? false,
      updatedAt:
          json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
      createdAt:
          json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
      description: json["description"] == null
          ? BilingualText(ar: '', en: '')
          : BilingualText.fromMap(json["description"]),
      note: json["note"] == null
          ? BilingualText(ar: '', en: '')
          : BilingualText.fromMap(json["note"]),
    );
  }
}

class BilingualText {
  BilingualText({
    this.ar,
    this.en,
  });

  final String ar;
  final String en;

  factory BilingualText.fromJson(String str) =>
      BilingualText.fromMap(json.decode(str));

  factory BilingualText.fromMap(Map<String, dynamic> json) =>
      BilingualText(ar: json["ar"], en: json["en"]);
}

class Option {
  Option({
    this.id,
    this.ar,
    this.en,
    this.price,
  });

  final String id;
  final String ar;
  final String en;
  final Price price;

  factory Option.fromJson(String str) => Option.fromMap(json.decode(str));

  factory Option.fromMap(Map<String, dynamic> json) {
    if (json["_id"] == null) {
      throw Exception("Item option id can't be null");
    }

    if (json["ar"] == null) {
      throw Exception("Item option Arabic name can't be null");
    }

    if (json["en"] == null) {
      throw Exception("Item option English name can't be null");
    }

    if (json["price"] == null) {
      throw Exception("Item option price can't be null");
    }

    return Option(
      id: json["_id"],
      ar: json["ar"],
      en: json["en"],
      price: Price.fromMap(json["price"]),
    );
  }
}

class Price {
  Price({
    this.amount,
    this.currency,
  });

  final int amount;
  final String currency;

  factory Price.fromJson(String str) => Price.fromMap(json.decode(str));

  factory Price.fromMap(Map<String, dynamic> json) {
    if (json["amount"] == null) {
      throw Exception("Price amount can't be null");
    }

    if (json["currency"] == null) {
      throw Exception("Price currency can't be null");
    }

    return Price(
      amount: json["amount"],
      currency: json["currency"],
    );
  }
}
